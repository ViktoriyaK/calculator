# Pycalc
Pycalc is an extented string calculator written in python. It is implemented using recursive decent algorithm, a formal specification
of the grammar in the form of a EBNF can be found in calculator/calculator.py documentation.

Supported operations:

- Arithmetic ( + , - , * , / , // , % , ^ ) ( ^ is a power).
- Comparison ( < , <= , == , != , >= , > ).
- 2 built-in python functions: abs and round.
- All functions and constants from standard python module math (trigonometry, logarithms, etc.).
- Constants and functions from modules in modules_names. Functions and constants from user defined modules
have a higher priority in case of name conflict then stuff from math module or built-in functions.
- Implicit multiplication.

## Installation

Clone the repository:

    $ git clone https://ViktoriyaK@bitbucket.org/ViktoriyaK/calculator.git

Install pycalc:

    $ cd calculator
    $ python setup.py install --user

## Running tests

In calculator/:

    $ python -m unittest tests.test_calculator -v

## How to use

    $ pycalc --help
    usage: pycalc [-h] [-m [MODULE [MODULE ...]]] EXPRESSION

    Pure-python command-line calculator.

    positional arguments:
      EXPRESSION            expression string to evaluate

    optional arguments:
      -h, --help            show this help message and exit
      -m [MODULE [MODULE ...]], --use-modules [MODULE [MODULE ...]]
                            additional modules to use
## Examples

    $ pycalc '1 + 2pi'
    $ 7.283185307179586

    $ pycalc 'sin(pi/2)'
    $ 1.0

    $ pycalc 'pi > e'
    $ True

    $ pycalc 'time() / 1e5' -m time
    $ 15629.665138826278

    $ pycalc 'func'
    $ ERROR: unknown name "func"

## Notes about using user defined python modules
Names of modules must be accessible via python standard module search paths.
Functions and constants from user defined modules have higher priority in case of name conflict then stuff from math module or built-in functions.