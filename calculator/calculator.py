import logging
from .tokenizer import Tokenizer


class ExpressionEvaluator:
    """Implementation of a string calculator based on recursive decent algorithm."""

    def calculate(self, text, modules_names=None):
        """
        Calculate a given expression.

        Supported operations:
        - Arithmetic ( + , - , * , / , // , % , ^ ) ( ^ is a power).
        - Comparison ( < , <= , == , != , >= , > ).
        - 2 built-in python functions: abs and round.
        - All functions and constants from standard python module math (trigonometry, logarithms, etc.).
        - Constants and functions from modules in modules_names. Functions and constants from user defined modules
        have a higher priority in case of name conflict then stuff from math module or built-in functions.
        - Implicit multiplication.

        :param text: Expression string to calculate.
        :param modules_names: List of module names which contain constants and methods to use
        if ones are founded in the expression.
        :return: Result of expression evaluation.
        """
        logging.debug('Enter calculate method with parameters: text=%s, module_names=%s', text, modules_names)

        self._tokens = Tokenizer().generate_tokens(text, modules_names)
        self._token = None
        self._next_token = None

        self._go_to_next_token()
        expression_value = self.comparison()
        if self._next_token:
            raise SyntaxError(f'an operation is missed after "{self._token.value}"')

        logging.debug('Exiting calculate method, expression value=%s', expression_value)
        return expression_value

    def _go_to_next_token(self):
        self._token, self._next_token = self._next_token, next(self._tokens, None)
        logging.debug('Current token - %s', self._token)

    def _accept_next_token(self, token_type):
        if self._next_token and self._next_token.type == token_type:
            self._go_to_next_token()
            return True
        else:
            return False

    def _check_token(self, token_type):
        if not self._accept_next_token(token_type):
            raise SyntaxError(f'"{token_type}" expected')

    def comparison(self):
        """comparison = plus_minus { ('>' | '>=' | '<' | '<=' | '==' | '!=')
                                    plus_minus }
        """
        expression_value = self.plus_minus()
        while (self._accept_next_token('GT') or self._accept_next_token('GE')
               or self._accept_next_token('LT') or self._accept_next_token('LE')
               or self._accept_next_token('EQ') or self._accept_next_token('NE')):
            operation = self._token.type
            right_expression_value = self.plus_minus()
            if operation == 'GT':
                expression_value = expression_value > right_expression_value
            elif operation == 'GE':
                expression_value = expression_value >= right_expression_value
            elif operation == 'LT':
                expression_value = expression_value < right_expression_value
            elif operation == 'LE':
                expression_value = expression_value <= right_expression_value
            elif operation == 'EQ':
                expression_value = expression_value == right_expression_value
            elif operation == 'NE':
                expression_value = expression_value != right_expression_value
        return expression_value

    def plus_minus(self):
        """plus_minus = mul_div { ('+' | '-') mul_div }"""
        expression_value = self.mul_div()
        while self._accept_next_token('PLUS') or self._accept_next_token('MINUS'):
            operation = self._token.type
            right_expression_value = self.mul_div()
            if operation == 'PLUS':
                expression_value += right_expression_value
            elif operation == 'MINUS':
                expression_value -= right_expression_value
        return expression_value

    def mul_div(self):
        """mul_div = pow { ('*' | '/' | '//' | '%') pow }"""
        expression_value = self.pow()
        while (self._accept_next_token('MUL') or self._accept_next_token('FLOORDIV')
               or self._accept_next_token('DIV') or self._accept_next_token('MOD')):
            operation = self._token.type
            right_expression_value = self.pow()
            if operation == 'MUL':
                expression_value *= right_expression_value
            if operation == 'FLOORDIV':
                expression_value //= right_expression_value
            if operation == 'DIV':
                expression_value /= right_expression_value
            elif operation == 'MOD':
                expression_value %= right_expression_value
        return expression_value

    def pow(self):
        """pow = number_or_plus_minus { ('^') number_or_plus_minus }"""
        expression_value = self.number_or_plus_minus()
        if self._accept_next_token('POW'):
            right_expression_value = self.pow()
            expression_value **= right_expression_value
        return expression_value

    def number_or_plus_minus(self):
        """number_or_plus_minus = NUMBER
            | FUNC
            | ( plus_minus )
        """
        if self._accept_next_token('MINUS'):
            return -self.number_or_plus_minus()

        elif self._accept_next_token('PLUS'):
            return self.number_or_plus_minus()

        elif self._accept_next_token('NUMBER'):
            if '.' in str(self._token.value) or 'e' in str(self._token.value):
                return float(self._token.value)
            else:
                return int(self._token.value)

        elif self._accept_next_token('FUNC'):
            func = self._token.value.func
            self._check_token('LPAREN')
            if self._accept_next_token('RPAREN'):
                return func()
            else:
                expression_values = [self.comparison()]
                while self._accept_next_token('COMMA'):
                    expression_values.append(self.comparison())
                self._check_token('RPAREN')
                return func(*expression_values)

        elif self._accept_next_token('LPAREN'):
            expression_value = self.comparison()
            self._check_token('RPAREN')
            return expression_value

        else:
            raise SyntaxError('number, function or expression expected')
