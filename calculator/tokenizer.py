import re
from collections import namedtuple
from importlib import import_module


Token = namedtuple('Token', ['type', 'value'])

NAME = r'(?P<NAME>[a-zA-Z_][a-zA-Z_0-9]*)'
NUMBER = r'(?P<NUMBER>(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?)'
POW = r'(?P<POW>\^)'
PLUS = r'(?P<PLUS>\+)'
MINUS = r'(?P<MINUS>-)'
MUL = r'(?P<MUL>\*)'
FLOORDIV = r'(?P<FLOORDIV>\/\/)'
DIV = r'(?P<DIV>\/)'
MOD = r'(?P<MOD>\%)'
GT = r'(?P<GT>>)'
GE = r'(?P<GE>>=)'
LT = r'(?P<LT><)'
LE = r'(?P<LE><=)'
EQ = r'(?P<EQ>==)'
NE = r'(?P<NE>!=)'
LPAREN = r'(?P<LPAREN>\()'
RPAREN = r'(?P<RPAREN>\))'
COMMA = r'(?P<COMMA>,)'
WS = r'(?P<WS>\s+)'

pattern = re.compile('|'.join([
    NAME,
    NUMBER,
    POW,
    PLUS,
    MINUS,
    MUL,
    FLOORDIV,
    DIV,
    MOD,
    GE,
    GT,
    LE,
    LT,
    EQ,
    NE,
    LPAREN,
    RPAREN,
    COMMA,
    WS
]))


class Tokenizer:
    """Implementation of a calculator parser."""

    def generate_tokens(self, text, modules_names=None):
        """
        Yield tokens, parsed from given expression string.

        Supported tokens:
        - Arithmetic ( + , - , * , / , // , % , ^ ) (^ is a power).
        - Comparison ( < , <= , == , != , >= , > ).
        - 2 built-in python functions: abs and round.
        - All functions and constants from standard python module math (trigonometry, logarithms, etc.).
        - Constants and functions from modules in modules_names. Functions and constants from user defined modules
        have a higher priority in case of name conflict then stuff from math module or built-in functions.
        - In case of implicit multiplication, generate missing multiplication sign.

        :param text: Expression string to parsed.
        :param modules_names: List of module names which contain constants and methods to use.
        :yield: The next token parsed from the given expression string.
        """
        self._init_functions_and_constants_lists(modules_names)

        scanner = pattern.scanner(text)
        previous_token_type = None
        for match in iter(scanner.match, None):
            token = Token(match.lastgroup, match.group())

            if token.type == 'WS':
                continue

            # in case of '...)(...' or '...)number...' or '...)func...'
            # or '...number(...' or '...number func...' yield missing multiplication sign
            if (previous_token_type == 'RPAREN'
                    and (token.type == 'LPAREN' or token.type == 'NUMBER' or token.type == 'NAME')
                    or previous_token_type == 'NUMBER' and (token.type == 'LPAREN' or token.type == 'NAME')):
                yield Token('MUL', '*')

            if token.type == 'NAME':
                yield self._get_function_or_constant_by_name(token.value)
            else:
                yield token

            previous_token_type = token.type

    def _init_functions_and_constants_lists(self, modules_names):
        import inspect
        import math
        from numbers import Number

        modules = [math]
        for module_name in modules_names or []:
            modules.append(import_module(module_name))
        modules.reverse()

        self._functions = {}
        self._constants = {}
        for module in modules:
            self._functions.update({func_name: func for func_name, func in inspect.getmembers(module)
                                    if (inspect.isbuiltin(func) or inspect.isfunction(func))
                                    and func_name not in self._functions})
            self._constants.update({const_name: getattr(module, const_name) for const_name in dir(module)
                                    if isinstance(getattr(module, const_name), Number)
                                    and const_name not in self._constants})

        if 'abs' not in self._functions:
            self._functions.update({'abs': abs})
        if 'round' not in self._functions:
            self._functions.update({'round': round})

    def _get_function_or_constant_by_name(self, name):
        Function = namedtuple('Function', ['func_name', 'func'])

        if name in self._functions:
            return Token('FUNC', Function(name, self._functions[name]))

        elif name in self._constants:
            return Token('NUMBER', self._constants[name])

        else:
            raise SyntaxError(f'unknown name "{name}"')
