import argparse
import logging.config
import sys
from os import path
from calculator.calculator import ExpressionEvaluator


logging.config.fileConfig(path.join(path.dirname(path.abspath(__file__)), 'configuration/logconfig.ini'))


def main():
    parser = argparse.ArgumentParser(prog='pycalc',
                                     description='Pure-python command-line calculator.')
    parser.add_argument('expression',
                        metavar='EXPRESSION',
                        type=str,
                        help='expression string to evaluate')
    parser.add_argument('-m',
                        '--use-modules',
                        metavar='MODULE',
                        nargs='*',
                        default=[],
                        help='additional modules to use')
    args = parser.parse_args()
    try:
        expression_value = ExpressionEvaluator().calculate(args.expression, args.use_modules)
    except (ModuleNotFoundError, SyntaxError) as e:
        print('ERROR:', e)
        logging.error(e, exc_info=True)
        sys.exit(2)
    else:
        print(expression_value)
