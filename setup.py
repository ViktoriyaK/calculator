from setuptools import setup, find_packages


setup(
    name='pycalc',
    version='0.1',
    author='Viktoriya Kaleda',
    packages=find_packages(),
    install_requires=[],
    entry_points='''
        [console_scripts]
        pycalc=console.pycalc:main'''
)
