import unittest
from calculator.calculator import ExpressionEvaluator
from collections import namedtuple
from itertools import chain
from math import *


TestSuit = namedtuple('TestSuit', ['text', 'expected_result'])

UNARY_OPERATORS = {
    "-13": -13,
    "6-(-13)": 6-(-13),
    "1---1": 1---1,
    "-+---+-1": -+---+-1,
}

OPERATION_PRIORITY = {
    "1+2*2": 1+2*2,
    "1+(2+3*2)*3": 1+(2+3*2)*3,
    "10*(2+1)": 10*(2+1),
    "10^(2+1)": 10**(2+1),
    "100/3^2": 100 / 3**2,
    "100/3%2^2": 100 / 3 % 2**2,
}

FUNCTIONS_AND_CONSTANTS = {
    "pi+e": pi+e,
    "log(e)": log(e),
    "sin(pi/2)": sin(pi/2),
    "log10(100)": log10(100),
    "sin(pi/2)*111*6": sin(pi/2)*111*6,
    "2*sin(pi/2)": 2*sin(pi/2),
    "pow(2, 3)": pow(2, 3),
    "abs(-5)": abs(-5),
    "round(123.4567890)": round(123.4567890),
}

ASSOCIATIVE = {
    "102%12%7": 102%12%7,
    "100/4/3": 100/4/3,
    "2^3^4": 2**3**4,
}

COMPARISON_OPERATORS = {
    "1+2*3==1+2*3": eval("1+2*3==1+2*3"),
    "e^5>=e^5+1": eval("e**5>=e**5+1"),
    "1+2*4/3+1!=1+2*4/3+2": eval("1+2*4/3+1!=1+2*4/3+2"),
}

COMMON_TESTS = {
    "(100)": (100),
    "666": 666,
    "-.1": -.1,
    "1/3": 1/3,
    "1.0/3.0": 1.0/3.0,
    ".1 * 2.0^56.0": .1 * 2.0**56.0,
    "e^34": e**34,
    "(2.0^(pi/pi+e/e+2.0^0.0))": (2.0**(pi/pi+e/e+2.0**0.0)),
    "(2.0^(pi/pi+e/e+2.0^0.0))^(1.0/3.0)": (2.0**(pi/pi+e/e+2.0**0.0))**(1.0/3.0),
    "sin(pi/2^1) + log(1*4+2^2+1, 3^2)": sin(pi/2**1) + log(1*4+2**2+1, 3**2),
    "10*e^0*log10(.4 -5/ -0.1-10) - -abs(-53/10) + -5": 10*e**0*log10(.4 -5/ -0.1-10) - -abs(-53/10) + -5,
    "sin(-cos(-sin(3.0)-cos(-sin(-3.0*5.0)-sin(cos(log10(43.0))))+cos(sin(sin(34.0-2.0^2.0))))--cos(1.0)--cos(0.0)^3.0)":
        sin(-cos(-sin(3.0)-cos(-sin(-3.0*5.0)-sin(cos(log10(43.0))))+cos(sin(sin(34.0-2.0**2.0))))--cos(1.0)--cos(0.0)**3.0),
    "2.0^(2.0^2.0*2.0^2.0)": 2.0**(2.0**2.0*2.0**2.0),
    "sin(e^log(e^e^sin(23.0),45.0) + cos(3.0+log10(e^-e)))": sin(e**log(e**e**sin(23.0),45.0) + cos(3.0+log10(e**-e))),
}

IMPLICIT_MULTIPLICATION = {
    '(1 + 2)3': (1 + 2) * 3,
    '(1 + 2)(3)': (1 + 2) * (3),
    '(1 + 2)(3 * 1)': (1 + 2) * (3 * 1),
    '5(1 + 2)': 5 * (1 + 2),
    '(5)(1 + 2)': (5) * (1 + 2),
    '(5 * 1) (1 + 2)': (5 * 1) * (1 + 2),
    "2sin(pi/2)": 2 * sin(pi/2),
    "2 sin(pi/2)": 2 * sin(pi/2),
    "sin(pi)sin(pi) + cos(pi)cos(pi)": sin(pi) * sin(pi) + cos(pi) * cos(pi),
}

ERROR_CASES = [
    "",
    "+",
    "1-",
    "1 2",
    "==7",
    "1 + 2(3 * 4))",
    "((1+2)",
    "1 + 1 2 3 4 5 6 ",
    "log100(100)",
    "------",
    "5 > = 6",
    "5 / / 6",
    "6 < = 6",
    "6 * * 6",
    "(((((",
    "abs",
    "pow(2, 3, 4)",
    "pow("
]


class CalculatorTest(unittest.TestCase):

    def setUp(self):
        self.calculator = ExpressionEvaluator()

    def test_calculate_expression_without_mistakes(self):
        test_cases = dict(chain.from_iterable(d.items() for d in ((
            UNARY_OPERATORS,
            OPERATION_PRIORITY,
            FUNCTIONS_AND_CONSTANTS,
            ASSOCIATIVE,
            COMPARISON_OPERATORS,
            COMMON_TESTS,
            IMPLICIT_MULTIPLICATION,
        ))))
        for text, expected_result in test_cases.items():
            self.assertEqual(self.calculator.calculate(text), expected_result, f'text: {text}')

    def test_calculate_expression_with_errors(self):
        for text in ERROR_CASES:
            with self.assertRaises(Exception):
                self.calculator.calculate(text)


if __name__ == '__main__':
    unittest.main()
